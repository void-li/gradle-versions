/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
rootProject.name = "gradle-versions"

gradle.projectsLoaded {
  rootProject.extra["publishing_mavenapi"] = System.getenv("CI_API_V4_URL")
  rootProject.extra["publishing_mavenpid"] = System.getenv("CI_PROJECT_ID")

  rootProject.extra["publishing_username"] =                  "Job-Token"
  rootProject.extra["publishing_password"] = System.getenv("CI_JOB_TOKEN" )
}

pluginManagement {
  repositories {
    gradlePluginPortal()

    maven { // gradle-version
      url = uri("https://gitlab.com/api/v4/projects/32058643/packages/maven")
    }

    maven { // gradle-versions
      url = uri("https://gitlab.com/api/v4/projects/32058644/packages/maven")
    }
  }

  resolutionStrategy {
    eachPlugin {
      if (requested.id.id.startsWith("li.void_.gradle.")) {
        useModule("${requested.id.id.reversed().replaceFirst('.', ':').reversed()}:${requested.version}")
      }
    }
  }
}

include(":GradleVersions")
project(":GradleVersions").name = "gradle-versions"
