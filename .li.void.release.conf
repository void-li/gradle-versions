; Before

[gitconfig]
handles = Before

user.name  = Release Bot
user.email = release@void.li

;

[verifier]
options = HEAD

;

[curr_version_extractor]
curr_version_extractor = true

[prev_version_extractor]
prev_version_extractor = true

; Commit

[explicit_bumper:Major]
explicit_bump = Major

match_subject = ^\\[major-release\\]$
match_message =

[explicit_bumper:Minor]
explicit_bump = Minor

match_subject = ^\\[minor-release\\]$
match_message =

[explicit_bumper:Patch]
explicit_bump = Patch

match_subject = ^\\[patch-release\\]$
match_message =

;

[implicit_bumper:Major]
implicit_bump = Major

match_subject = ^major:
match_message =

[implicit_bumper:Minor]
implicit_bump = Minor

match_subject = ^minor:
match_message =

[implicit_bumper:Patch]
implicit_bump = Patch

match_subject = ^patch:
match_message =

;

[changelog_generator:Major]
match_subject = ^major:
group         = Major Changes

[changelog_generator:Minor]
match_subject = ^minor:
group         = Minor Changes

[changelog_generator:Patch]
match_subject = ^patch:
group         = Patches

; Beyond

[changelog_converter:Major]
match_subject = ^major:[ ]*(?<subject>.*)$
match_subject_format = %{subject}

[changelog_converter:Minor]
match_subject = ^minor:[ ]*(?<subject>.*)$
match_subject_format = %{subject}

[changelog_converter:Patch]
match_subject = ^patch:[ ]*(?<subject>.*)$
match_subject_format = %{subject}

;

[next_version_generator]
next_version_generator = true

;

[changelog_extractor]
changelog = CHANGELOG.md

title_starts = ^##[ ]
title_parser = ^##[ ]Changes in v(?<major>[0-9]+).(?<minor>[0-9]+).(?<patch>[0-9]+)

group_starts = ^###[ ]
group_parser = ^###[ ](?<group>[^$]+)

entry_starts = ^- (?<entry>.+)
entry_inside = ^(  (?<entry>.+)|)$

[changelog_publisher]
changelog = CHANGELOG.md

title_starts = ^##[ ]
title_parser = ^##[ ]Changes in v(?<major>[0-9]+).(?<minor>[0-9]+).(?<patch>[0-9]+)

title_format =  ## Changes in v%{major}.%{minor}.%{patch}
group_format =  ### %{group}
entry_format =  - %{entry}

;

[committer]
options = %{version} --allow-empty CHANGELOG.md -S

[tagger]
options = %{version}

[pusher]
options = origin HEAD --tags
