/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files

class GradleVersionsPluginTest {
  private lateinit var tmpDir: File

  @BeforeEach
  fun setupTmpDir() {
    tmpDir = Files.createTempDirectory("tmp").toFile()
  }

  @Test
  fun plugin() {
    val tempFile = File(tmpDir, "versions.properties")

    tempFile.appendText("org.example.example = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    val project = ProjectBuilder.builder().withProjectDir(tmpDir).build()
    project.plugins.apply(PLUGIN_ID)

    val extension = project.extensions.findByType(GradleVersionsPlugin.GradleVersionsPluginExtension::class.java)
    if (extension == null) {
      Assertions.fail<Any>("Extension not found")
      return
    }

    Assertions.assertEquals("org.example:example:1.0.0",
      extension("org.example", "example")
    )

    Assertions.assertThrows(GradleVersionsPluginException::class.java) {
      extension("org.example", "missing")
    }

    Assertions.assertNotNull {
      project.tasks.findByName("searchDependencies")
    }

    Assertions.assertNotNull {
      project.tasks.findByName("updateDependencies")
    }
  }

  @AfterEach
  fun closeTmpDir() {
    if (tmpDir.exists()) {
      tmpDir.delete()
    }
  }

  companion object {
    const val PLUGIN_ID = "li.void_.gradle.gradle-versions"
  }
}
