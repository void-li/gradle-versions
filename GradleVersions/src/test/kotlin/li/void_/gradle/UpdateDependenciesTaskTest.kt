/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files

class UpdateDependenciesTaskTest {
  private lateinit var tmpDir: File

  @BeforeEach
  fun setupTmpDir() {
    tmpDir = Files.createTempDirectory("tmp").toFile()
  }

  @Test
  fun updateDependencies() {
    val tempFile = File(tmpDir, "versions.properties")

    tempFile.appendText("# update available: 2.0.0"   )
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleA = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleB = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("# update available: 3.0.0"   )
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleC = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    val updateDependenciesTask = UpdateDependenciesTask(TestLogger())
    updateDependenciesTask.execute(tempFile.path)

    val tempText = tempFile.readText()

    Assertions.assertEquals(
      """
        org.example.exampleA = 2.0.0
        org.example.exampleB = 1.0.0
        org.example.exampleC = 3.0.0
      """.trimIndent(),
      tempText.trim()
    )
  }

  @AfterEach
  fun closeTmpDir() {
    if (tmpDir.exists()) {
      tmpDir.delete()
    }
  }
}
