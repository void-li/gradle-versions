/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.slf4j.Logger
import org.slf4j.Marker

class TestLogger : Logger {
  override fun getName(): String {
    return this::class.java.name
  }

  override fun isTraceEnabled(): Boolean {
    return true
  }

  override fun isTraceEnabled(marker: Marker?): Boolean {
    return true
  }

  override fun trace(msg: String?) {
    log("T", msg)
  }

  override fun trace(format: String?, arg: Any?) {
    log("T", format, arg)
  }

  override fun trace(format: String?, arg1: Any?, arg2: Any?) {
    log("T", format, arg1, arg2)
  }

  override fun trace(format: String?, vararg arguments: Any?) {
    log("T", format, *arguments)
  }

  override fun trace(msg: String?, t: Throwable?) {
    log("T", msg, t)
  }

  override fun trace(marker: Marker?, msg: String?) {
    log("T", msg)
  }

  override fun trace(marker: Marker?, format: String?, arg: Any?) {
    log("T", format, arg)
  }

  override fun trace(marker: Marker?, format: String?, arg1: Any?, arg2: Any?) {
    log("T", format, arg1, arg2)
  }

  override fun trace(marker: Marker?, format: String?, vararg arguments: Any?) {
    log("T", format, *arguments)
  }

  override fun trace(marker: Marker?, msg: String?, t: Throwable?) {
    log("T", msg, t)
  }

  override fun isDebugEnabled(): Boolean {
    return true
  }

  override fun isDebugEnabled(marker: Marker?): Boolean {
    return true
  }

  override fun debug(msg: String?) {
    log("D", msg)
  }

  override fun debug(format: String?, arg: Any?) {
    log("D", format, arg)
  }

  override fun debug(format: String?, arg1: Any?, arg2: Any?) {
    log("D", format, arg1, arg2)
  }

  override fun debug(format: String?, vararg arguments: Any?) {
    log("D", format, *arguments)
  }

  override fun debug(msg: String?, t: Throwable?) {
    log("D", msg, t)
  }

  override fun debug(marker: Marker?, msg: String?) {
    log("D", msg)
  }

  override fun debug(marker: Marker?, format: String?, arg: Any?) {
    log("D", format, arg)
  }

  override fun debug(marker: Marker?, format: String?, arg1: Any?, arg2: Any?) {
    log("D", format, arg1, arg2)
  }

  override fun debug(marker: Marker?, format: String?, vararg arguments: Any?) {
    log("D", format, *arguments)
  }

  override fun debug(marker: Marker?, msg: String?, t: Throwable?) {
    log("D", msg, t)
  }

  override fun isInfoEnabled(): Boolean {
    return true
  }

  override fun isInfoEnabled(marker: Marker?): Boolean {
    return true
  }

  override fun info(msg: String?) {
    log("I", msg)
  }

  override fun info(format: String?, arg: Any?) {
    log("I", format, arg)
  }

  override fun info(format: String?, arg1: Any?, arg2: Any?) {
    log("I", format, arg1, arg2)
  }

  override fun info(format: String?, vararg arguments: Any?) {
    log("I", format, *arguments)
  }

  override fun info(msg: String?, t: Throwable?) {
    log("I", msg, t)
  }

  override fun info(marker: Marker?, msg: String?) {
    log("I", msg)
  }

  override fun info(marker: Marker?, format: String?, arg: Any?) {
    log("I", format, arg)
  }

  override fun info(marker: Marker?, format: String?, arg1: Any?, arg2: Any?) {
    log("I", format, arg1, arg2)
  }

  override fun info(marker: Marker?, format: String?, vararg arguments: Any?) {
    log("I", format, *arguments)
  }

  override fun info(marker: Marker?, msg: String?, t: Throwable?) {
    log("I", msg, t)
  }

  override fun isWarnEnabled(): Boolean {
    return true
  }

  override fun isWarnEnabled(marker: Marker?): Boolean {
    return true
  }

  override fun warn(msg: String?) {
    log("W", msg)
  }

  override fun warn(format: String?, arg: Any?) {
    log("W", format, arg)
  }

  override fun warn(format: String?, arg1: Any?, arg2: Any?) {
    log("W", format, arg1, arg2)
  }

  override fun warn(format: String?, vararg arguments: Any?) {
    log("W", format, *arguments)
  }

  override fun warn(msg: String?, t: Throwable?) {
    log("W", msg, t)
  }

  override fun warn(marker: Marker?, msg: String?) {
    log("W", msg)
  }

  override fun warn(marker: Marker?, format: String?, arg: Any?) {
    log("W", format, arg)
  }

  override fun warn(marker: Marker?, format: String?, arg1: Any?, arg2: Any?) {
    log("W", format, arg1, arg2)
  }

  override fun warn(marker: Marker?, format: String?, vararg arguments: Any?) {
    log("W", format, *arguments)
  }

  override fun warn(marker: Marker?, msg: String?, t: Throwable?) {
    log("W", msg, t)
  }

  override fun isErrorEnabled(): Boolean {
    return true
  }

  override fun isErrorEnabled(marker: Marker?): Boolean {
    return true
  }

  override fun error(msg: String?) {
    log("E", msg)
  }

  override fun error(format: String?, arg: Any?) {
    log("E", format, arg)
  }

  override fun error(format: String?, arg1: Any?, arg2: Any?) {
    log("E", format, arg1, arg2)
  }

  override fun error(format: String?, vararg arguments: Any?) {
    log("E", format, *arguments)
  }

  override fun error(msg: String?, t: Throwable?) {
    log("E", msg, t)
  }

  override fun error(marker: Marker?, msg: String?) {
    log("E", msg)
  }

  override fun error(marker: Marker?, format: String?, arg: Any?) {
    log("E", format, arg)
  }

  override fun error(marker: Marker?, format: String?, arg1: Any?, arg2: Any?) {
    log("E", format, arg1, arg2)
  }

  override fun error(marker: Marker?, format: String?, vararg arguments: Any?) {
    log("E", format, *arguments)
  }

  override fun error(marker: Marker?, msg: String?, t: Throwable?) {
    log("E", msg, t)
  }

  private fun log(level: String, msg: String?) {
    System.err.println("[$level] $msg")
  }

  private fun log(level: String, format: String?, arg: Any?) {
    System.err.println("[$level] " + String.format(format!!.replace("{}", "%s"), arg))
  }

  private fun log(level: String, format: String?, arg1: Any?, arg2: Any?) {
    System.err.println("[$level] " + String.format(format!!.replace("{}", "%s"), arg1, arg2))
  }

  private fun log(level: String, format: String?, vararg arguments: Any?) {
    System.err.println("[$level] " + String.format(format!!.replace("{}", "%s"), *arguments))
  }

  private fun log(level: String, msg: String?, t: Throwable?) {
    System.err.println("[$level] $msg: $t")
  }
}
