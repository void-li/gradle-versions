/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class VersionTest {
  @Test
  fun construct() {
    Version("42.23.7")
  }

  @Test
  fun constructThrowsOnIllegalPattern() {
    Assertions.assertThrows(IllegalArgumentException::class.java) {
      Version("")
    }

    Assertions.assertThrows(IllegalArgumentException::class.java) {
      Version("a")
    }
  }

  @Test
  fun equally() {
    Assertions.assertTrue(
      Version("42"  ) == Version("42"  )
    )

    Assertions.assertTrue(
      Version("42"  ) == Version("42.0")
    )

    Assertions.assertTrue(
      Version("42.0") == Version("42"  )
    )

    Assertions.assertTrue(
      Version("42.0") == Version("42.0")
    )
  }

  @Test
  fun smaller() {
    Assertions.assertTrue(
      Version("42") < Version( "43"  )
    )

    Assertions.assertTrue(
      Version("42") < Version("343"  )
    )

    Assertions.assertTrue(
      Version("42") < Version( "42.1")
    )
  }

  @Test
  fun greater() {
    Assertions.assertTrue(
      Version( "43"  ) > Version("42")
    )

    Assertions.assertTrue(
      Version("343"  ) > Version("42")
    )

    Assertions.assertTrue(
      Version( "42.1") > Version("42")
    )
  }

  @Test
  fun qualify() {
    Assertions.assertTrue(
      Version("42-a") < Version("42"  )
    )

    Assertions.assertTrue(
      Version("42"  ) > Version("42-a")
    )

    Assertions.assertTrue(
      Version("42-a") < Version("42-b")
    )

    Assertions.assertTrue(
      Version("42-b") > Version("42-a")
    )
  }
}
