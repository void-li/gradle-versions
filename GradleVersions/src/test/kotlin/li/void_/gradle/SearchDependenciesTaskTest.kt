/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class SearchDependenciesTaskTest {
  private lateinit var tmpDir: File

  @BeforeEach
  fun setupTmpDir() {
    tmpDir = Files.createTempDirectory("tmp").toFile()
  }

  @Test
  fun searchDependenciesInMavenRepository() {
    val tempFile = File(tmpDir, "versions.properties")

    tempFile.appendText("org.apache.commons.commons-math = 2.0")
    tempFile.appendText(System.lineSeparator())

    val searchDependenciesTask = SearchDependenciesTask(TestLogger())
    searchDependenciesTask.execute(tempFile.path) { listOf(MAVEN_REPOSITORY) }

    val tempText = tempFile.readText()

    Assertions.assertEquals(
      """
        # update available: 2.2
        org.apache.commons.commons-math = 2.0
      """.trimIndent(),
      tempText.trim()
    )
  }

  @Test
  fun searchDependenciesInLocalRepository() {
    val tempFile = File(tmpDir, "versions.properties")

    tempFile.appendText("org.example.exampleA = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleB = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleC = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    val searchDependenciesTask = SearchDependenciesTask(TestLogger())
    searchDependenciesTask.execute(tempFile.path) { listOf(LOCAL_REPOSITORY) }

    val tempText = tempFile.readText()

    Assertions.assertEquals(
      """
        # update available: 2.0.0
        org.example.exampleA = 1.0.0
        org.example.exampleB = 1.0.0
        # update available: 3.0.0
        org.example.exampleC = 1.0.0
      """.trimIndent(),
      tempText.trim()
    )
  }

  @Test
  fun searchDependenciesInLocalRepositoryAfterMavenRepository() {
    val tempFile = File(tmpDir, "versions.properties")

    tempFile.appendText("org.example.exampleA = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleB = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleC = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    val searchDependenciesTask = SearchDependenciesTask(TestLogger())
    searchDependenciesTask.execute(tempFile.path) { listOf(MAVEN_REPOSITORY, LOCAL_REPOSITORY) }

    val tempText = tempFile.readText()

    Assertions.assertEquals(
      """
        # update available: 2.0.0
        org.example.exampleA = 1.0.0
        org.example.exampleB = 1.0.0
        # update available: 3.0.0
        org.example.exampleC = 1.0.0
      """.trimIndent(),
      tempText.trim()
    )
  }

  @Test
  fun searchDependenciesInLocalRepositoryAfterLocalRepositoryNotFound() {
    val tempFile = File(tmpDir, "versions.properties")

    tempFile.appendText("org.example.exampleA = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleB = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleC = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    val searchDependenciesTask = SearchDependenciesTask(TestLogger())
    searchDependenciesTask.execute(tempFile.path) { listOf(EMPTY_REPOSITORY, LOCAL_REPOSITORY) }

    val tempText = tempFile.readText()

    Assertions.assertEquals(
      """
        # update available: 2.0.0
        org.example.exampleA = 1.0.0
        org.example.exampleB = 1.0.0
        # update available: 3.0.0
        org.example.exampleC = 1.0.0
      """.trimIndent(),
      tempText.trim()
    )
  }

  @Test
  fun searchDependenciesNotFound() {
    val tempFile = File(tmpDir, "versions.properties")

    tempFile.appendText("org.example.exampleA = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleB = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("org.example.exampleC = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    Assertions.assertThrows(GradleVersionsPluginException::class.java) {
      val searchDependenciesTask = SearchDependenciesTask(TestLogger())
      searchDependenciesTask.execute(tempFile.path) { listOf(EMPTY_REPOSITORY) }
    }
  }

  @Test
  fun searchDependenciesKeepsComments() {
    val tempFile = File(tmpDir, "versions.properties")

    tempFile.appendText("# I'm a comment")
    tempFile.appendText(System.lineSeparator())
    tempFile.appendText("org.example.exampleA = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("# I'm a comment")
    tempFile.appendText(System.lineSeparator())
    tempFile.appendText("org.example.exampleB = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("# I'm a comment")
    tempFile.appendText(System.lineSeparator())
    tempFile.appendText("# I'm a comment")
    tempFile.appendText(System.lineSeparator())
    tempFile.appendText("org.example.exampleC = 1.0.0")
    tempFile.appendText(System.lineSeparator())

    tempFile.appendText("# I'm a comment")
    tempFile.appendText(System.lineSeparator())

    val searchDependenciesTask = SearchDependenciesTask(TestLogger())
    searchDependenciesTask.execute(tempFile.path) { listOf(LOCAL_REPOSITORY) }

    val tempText = tempFile.readText()

    Assertions.assertEquals(
      """
        # I'm a comment
        # update available: 2.0.0
        org.example.exampleA = 1.0.0
        # I'm a comment
        org.example.exampleB = 1.0.0
        # I'm a comment
        # I'm a comment
        # update available: 3.0.0
        org.example.exampleC = 1.0.0
        # I'm a comment
      """.trimIndent(),
      tempText.trim()
    )
  }

  @AfterEach
  fun closeTmpDir() {
    if (tmpDir.exists()) {
      tmpDir.delete()
    }
  }

    companion object {
    const val MAVEN_REPOSITORY = "https://repo.maven.apache.org/maven2/"
    /***/ val LOCAL_REPOSITORY = "file://${Paths.get(".").toAbsolutePath()}/src/test/resources/SearchDependenciesTaskTestData/"
    /***/ val EMPTY_REPOSITORY = "file://${Paths.get(".").toAbsolutePath()}/src/test/resources/SearchDependenciesTaskTestVoid/"
  }
}
