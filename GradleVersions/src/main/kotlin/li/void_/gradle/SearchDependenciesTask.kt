/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.slf4j.Logger
import java.io.File
import java.io.FileNotFoundException
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class SearchDependenciesTask (
  private val logger: Logger
) {
  fun execute(versionsPropertiesPath: String, repositories: () -> List<String>) {
    logger.trace("{} execute '{}'",
      this::class.simpleName, versionsPropertiesPath
    )

    val httpClient = HttpClient.newHttpClient()

    val dependenciesReader = DependenciesReader(logger)
    dependenciesReader.execute(versionsPropertiesPath)

    val dependencies       = dependenciesReader.dependencies

    dependencyLoop@
    for (dependency in dependencies) {
      logger.debug("{} Search dependency '{}'.",
        this::class.simpleName, dependency
      )

      val dependencyItem = dependency.depends.replace('.', '/')

      repositoryLoop@
      for (repository in repositories()) {
        logger.debug("{} Search repository '{}'.",
          this::class.simpleName, repository
        )

        val repositoryItem = repository.trimEnd('/')

        try {
          val mavenMetadata: String

          if (repositoryItem.startsWith("file://")) {
            val mavenMetadataPath = "$repositoryItem/$dependencyItem/maven-metadata-local.xml".substringAfter("file://")
            logger.debug("{} Search repository maven metadata path: '{}'.",
              this::class.simpleName, mavenMetadataPath
            )

            try {
              mavenMetadata = File(mavenMetadataPath).readText()
            }
            catch (e: FileNotFoundException) {
              logger.debug("{} File not found: '{}'.",
                this::class.simpleName, mavenMetadataPath
              )

              continue@repositoryLoop
            }
          } else {
            val mavenMetadataPath = "$repositoryItem/$dependencyItem/maven-metadata.xml"
            logger.debug("{} Search repository maven metadata path: '{}'.",
              this::class.simpleName, mavenMetadataPath
            )

            val httpRequest = HttpRequest.newBuilder().uri(
              URI.create(mavenMetadataPath)
            ).build()

            val httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString())
            if (httpResponse.statusCode() != 200) {
              logger.debug("{} Invalid http response: '{}' '{}'.",
                this::class.simpleName, httpResponse.statusCode(), httpResponse.body()
              )

              continue@repositoryLoop
            }

            mavenMetadata = httpResponse.body()
          }

          val xmlMapper = XmlMapper.builder()
            .addModule(        KotlinModule())
            .addModule(JaxbAnnotationModule())
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .defaultUseWrapper(false)
            .build()

          val xmlObject = xmlMapper.readValue(mavenMetadata, Artifact.Adapter::class.java)

          val version = xmlObject.versions.lastOrNull { !it.contains('-') }
          if (version != null) {
            if (Version(version) > Version(dependency.version)) {
              dependency.updates = version

              logger.info("{} Update dependency '{}' '{}' to version '{}'.",
                this::class.simpleName, dependency.depends, dependency.version, dependency.updates
              )
            }

            continue@dependencyLoop
          }
        }
        catch (e: Throwable) {
          logger.error(e.message, e)
        }
      }

      throw GradleVersionsPluginException("Unknown dependency '${dependency.depends}'")
    }

    val dependenciesWriter = DependenciesWriter(logger, dependencies)
    dependenciesWriter.execute(versionsPropertiesPath)
  }
}
