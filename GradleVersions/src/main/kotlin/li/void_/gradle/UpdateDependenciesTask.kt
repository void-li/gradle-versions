/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.slf4j.Logger

class UpdateDependenciesTask (
  private val logger: Logger
) {
  fun execute(versionsPropertiesPath: String) {
    logger.trace("{} execute '{}'",
      this::class.simpleName, versionsPropertiesPath
    )

    val dependenciesReader = DependenciesReader(logger)
    dependenciesReader.execute(versionsPropertiesPath)

    val dependencies       = dependenciesReader.dependencies

    dependencyLoop@
    for (dependency in dependencies) {
      logger.debug("{} Update dependency '{}'.",
        this::class.simpleName, dependency
      )

      val updates = dependency.updates
      if (updates != null) {
        logger.info("{} Update dependency '{}' '{}' to version '{}'.",
          this::class.simpleName, dependency.depends, dependency.version, dependency.updates
        )

        dependency.version = updates
        dependency.updates = null
      }
    }

    val dependenciesWriter = DependenciesWriter(logger, dependencies)
    dependenciesWriter.execute(versionsPropertiesPath)
  }
}
