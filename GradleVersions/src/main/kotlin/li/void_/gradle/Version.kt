/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import kotlin.math.min

class Version (string: String) : Comparable<Version> {
  private val version: List<String>
  private val variant:      String?

  init {
    val strings = string.split('-', limit = 2)

    if (   strings.        isEmpty()
        || strings.first().isEmpty()) {
      throw IllegalArgumentException(
        "Illegal version string '$string'"
      )
    }

    this.version = strings.getOrNull(0)!!.split('.')
    this.variant = strings.getOrNull(1)

    this.version.forEach {
      if (it.toIntOrNull() == null) {
        throw IllegalArgumentException(
          "Illegal version string '$string' '$it'"
        )
      }
    }
  }

  override fun equals(other: Any?): Boolean {
    if (other !== this) {
      if (other !is Version) {
        return false
      }

      if (compareTo(other) != 0) {
        return false
      }
    }

    return true
  }

  override fun hashCode(): Int {
    var result = 5381
    result = 33 * result + version.hashCode()
    return result
  }

  override fun compareTo(other: Version): Int {
    val l = min(version.size, other.version.size)

    for (i in 0 until l) {
      val comparison = version[i].toInt().compareTo(other.version[i].toInt())
      if (comparison != 0) {
        return comparison
      }
    }

    for (i in l until this .version.size) {
      if (this .version[i].toInt() != 0) {
        return +1
      }
    }

    for (i in l until other.version.size) {
      if (other.version[i].toInt() != 0) {
        return -1
      }
    }

    val variantl = this .variant
    val variantr = other.variant

    return (
      if (variantl != null) {
        if (variantr != null) {
          variantl.compareTo(variantr)
        } else { -1 }
      } else {
        if (variantr != null) {
          +1
        } else { -0 }
      }
    )
  }
}
