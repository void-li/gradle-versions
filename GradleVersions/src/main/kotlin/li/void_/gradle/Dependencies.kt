/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.slf4j.Logger
import java.io.File
import java.io.FileReader
import java.io.FileWriter

abstract class Dependencies (
  private val logger: Logger,
  private val active: Boolean = false
) {
  private val currentBuilder = StringBuilder()
  private val commentBuilder = StringBuilder()

  private var commentPresent = false

  private val dependsBuilder = StringBuilder()
  private val versionBuilder = StringBuilder()

  fun execute(versionsPropertiesPath: String) {
    logger.trace("{} execute '{}'",
      this::class.simpleName, versionsPropertiesPath
    )

    var success = false

    try {
      var text: String

      @Suppress("UnnecessaryVariable")
      val fileReaderPath =   versionsPropertiesPath
      logger.debug("{} File reader path: '{}'.",
        this::class.simpleName, fileReaderPath
      )

      FileReader(fileReaderPath).use { fileReader ->
        text = fileReader.readText()
      }

      if (!text.endsWith("\n")) {
        text += "\n"
      }

      val fileWriterPath = "$versionsPropertiesPath.tmp"
      logger.debug("{} File writer path: '{}'.",
        this::class.simpleName, fileWriterPath
      )

      FileWriter(fileWriterPath).use { fileWriter ->
        var state = State.INITIAL

        loop@
        for (char in text) {
          currentBuilder.append(char)

          if (char == '\r') {
          //state = State.INITIAL NO!
            continue@loop
          }

          if (char == '\n') {
            val (currentRaw, _         ) = takeCurrent()

            val (dependsRaw, dependsStr) = takeDepends()
            val (versionRaw, versionStr) = takeVersion()

            if (dependsStr.isNotEmpty() && versionStr.isNotEmpty()) {
              val (commentRaw, _) = takeComment()

              if (commentPresent && !commentRaw.startsWith(UPDATE_AVAILABLE)) {
                fileWriter.write("#")
                fileWriter.write(commentRaw)

                fileWriter.write(
                  System.lineSeparator()
                )
              }

              val dependency: Dependency

              @Suppress("LiftReturnOrAssignment")
              if (commentPresent &&  commentRaw.startsWith(UPDATE_AVAILABLE)) {
                dependency = Dependency(dependsStr, versionStr, commentRaw.substringAfter(UPDATE_AVAILABLE).trim())
              } else {
                dependency = Dependency(dependsStr, versionStr)
              }

              commentPresent = false

              val dependence: Dependency = executeImpl(dependency)

              val updates = dependence.updates
              if (updates != null) {
                fileWriter.write("#")
                fileWriter.write(UPDATE_AVAILABLE)
                fileWriter.write(updates)

                fileWriter.write(
                  System.lineSeparator()
                )
              }

              val versionNew: String

              @Suppress("LiftReturnOrAssignment")
              if (dependence.version != dependency.version) {
                versionNew = " " + dependence.version
              } else {
                versionNew =                  versionRaw
              }

              fileWriter.write(dependsRaw)
              fileWriter.write("=")
              fileWriter.write(versionNew)

              fileWriter.write(
                System.lineSeparator()
              )
            }
            else {
              if (state != State.COMMENT) {
                if (commentPresent) {
                  commentPresent = false

                  val (commentRaw, _) = takeComment()

                  fileWriter.write("#")
                  fileWriter.write(commentRaw)

                  fileWriter.write(
                    System.lineSeparator()
                  )
                }

                fileWriter.write(currentRaw)
              }
            }

            state = State.INITIAL
            continue@loop
          }

          when (state) {
            State.INITIAL -> {
              if (char == '#') {
                if (commentPresent) {
                  commentPresent = false

                  val (commentRaw, _) = takeComment()

                  fileWriter.write("#")
                  fileWriter.write(commentRaw)

                  fileWriter.write(
                    System.lineSeparator()
                  )
                }

                commentPresent = true

                state = State.COMMENT
                continue@loop
              }

              if (char != ' ') {
                dependsBuilder.append(char)

                state = State.DEPENDS
                continue@loop
              }
            }

            State.COMMENT -> {
              commentBuilder.append(char)

              state = State.COMMENT
              continue@loop
            }

            State.DEPENDS -> {
              if (char == '=') {
                state = State.VERSION
                continue@loop
              }

              dependsBuilder.append(char)

              state = State.DEPENDS
              continue@loop
            }

            State.VERSION -> {
              versionBuilder.append(char)

              state = State.VERSION
              continue@loop
            }
          }
        }

        if (commentPresent) {
          commentPresent = false

          val (commentRaw, _) = takeComment()

          fileWriter.write("#")
          fileWriter.write(commentRaw)

          fileWriter.write(
            System.lineSeparator()
          )
        }
      }

      success = true
    }
    finally {
      val versionsPropertiesPtmp = "$versionsPropertiesPath.tmp"

      if (active && success) {
        logger.debug("{} Rename '{}' to '{}'.",
          this::class.simpleName, versionsPropertiesPtmp, versionsPropertiesPath
        )

        File(versionsPropertiesPtmp).renameTo(File(versionsPropertiesPath))
      } else {
        logger.debug("{} Remove '{}'.",
          this::class.simpleName, versionsPropertiesPtmp
        )

        File(versionsPropertiesPtmp).delete()
      }
    }
  }

  protected abstract fun executeImpl(dependency: Dependency): Dependency

  private fun takeCurrent(): Value {
    val currentRaw = currentBuilder.toString()
    currentBuilder.clear()

    val currentStr = currentRaw.trim()
    return Value(currentRaw, currentStr)
  }

  private fun takeComment(): Value {
    val commentRaw = commentBuilder.toString()
    commentBuilder.clear()

    val commentStr = commentRaw.trim()
    return Value(commentRaw, commentStr)
  }

  private fun takeDepends(): Value {
    val dependsRaw = dependsBuilder.toString()
    dependsBuilder.clear()

    val dependsStr = dependsRaw.trim()
    return Value(dependsRaw, dependsStr)
  }

  private fun takeVersion(): Value {
    val versionRaw = versionBuilder.toString()
    versionBuilder.clear()

    val versionStr = versionRaw.trim()
    return Value(versionRaw, versionStr)
  }

  private enum class State {
    INITIAL,
    COMMENT,
    DEPENDS,
    VERSION
  }

  private data class Value (
    val raw: String,
    val str: String
  )

  companion object {
    const val UPDATE_AVAILABLE = " update available: "
  }
}
