/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlElementWrapper
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.adapters.XmlAdapter
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter

@XmlRootElement(name = "metadata")
open class Artifact {
  @XmlElement       (name = "versioning")
//@XmlElementWrapper(name = "versioning")
  @XmlJavaTypeAdapter(VersionsAdapter::class)
  open var versions = mutableListOf<String>()

  private class Versions (
    @XmlElement(name = "version")
    @XmlElementWrapper
    val versions: MutableList<String>
  )

  private class VersionsAdapter : XmlAdapter<Versions, MutableList<String>>() {
    override fun   marshal(versions: MutableList<String>?): Versions {
      return Versions( versions ?: mutableListOf() )
    }

    override fun unmarshal(versions: Versions?): MutableList<String> {
      return versions?.versions ?: mutableListOf()
    }
  }

  class Adapter : Artifact() {
    @XmlElement       (name = "versioning")
    @XmlElementWrapper(name = "versioning")
    @XmlJavaTypeAdapter(VersionsAdapter::class)
    override var versions = mutableListOf<String>()
  }
}
