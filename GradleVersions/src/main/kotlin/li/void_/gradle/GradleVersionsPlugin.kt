/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import java.io.FileReader
import java.util.*

class GradleVersionsPlugin : Plugin<Project> {
  override fun apply(target: Project) {
    @Suppress("UnnecessaryVariable")
    val project = target

    project.logger.trace("{} apply '{}'",
      this::class.simpleName, project
    )

    project.extensions.create("dep",
      GradleVersionsPluginExtension::class.java, versionsProperties(project)
    )

    project.tasks.create("searchDependencies") { task ->
      task.group = "dependencies"

      task.doLast {
        val versionsPropertiesPath = versionsProperties(project)

        val searchDependenciesTask = SearchDependenciesTask(project.logger)
        searchDependenciesTask.execute(versionsPropertiesPath) {
          project.repositories.filterIsInstance<MavenArtifactRepository>().map { it.url.toString() }
        }
      }
    }

    project.tasks.create("updateDependencies") { task ->
      task.group = "dependencies"

      task.doLast {
        val versionsPropertiesPath = versionsProperties(project)

        val updateDependenciesTask = UpdateDependenciesTask(project.logger)
        updateDependenciesTask.execute(versionsPropertiesPath)
      }
    }
  }

  private fun versionsProperties(project: Project): String {
    return project.rootProject.projectDir.path + "/versions.properties"
  }

  open class GradleVersionsPluginExtension (
    versionsPath: String
  ) {
    private val versions = Properties()

    init {
      FileReader(versionsPath).use {
        versions.load(it)
      }
    }

    operator fun invoke(groupId: String, artifactId: String): String {
      val version = versions.getProperty("$groupId.$artifactId")
      if (version != null) {
        return "$groupId:$artifactId:$version"
      }

      throw GradleVersionsPluginException("Missing dependency '$groupId.$artifactId' version")
    }
  }
}
