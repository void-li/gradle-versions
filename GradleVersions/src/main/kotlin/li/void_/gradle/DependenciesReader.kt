/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
package li.void_.gradle

import org.slf4j.Logger

class DependenciesReader (
  logger: Logger,
  val dependencies: MutableList<Dependency> = mutableListOf()
) : Dependencies(logger) {
  override fun executeImpl(dependency: Dependency): Dependency {
    dependencies.add(dependency)

    return dependency
  }
}
